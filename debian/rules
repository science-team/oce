#!/usr/bin/make -f

CFLAGS   := $(shell dpkg-buildflags --get CPPFLAGS) $(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS := $(shell dpkg-buildflags --get CPPFLAGS) $(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS  := $(shell dpkg-buildflags --get LDFLAGS)

include /usr/share/dpkg/architecture.mk
UNAME := $(shell uname -m)
DEBUG_PATH := obj-$(DEB_HOST_GNU_TYPE)/Unix/$(UNAME)-RelWithDebInfo-$(DEB_HOST_ARCH_BITS)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

ifeq ($(findstring parallel=,$(DEB_BUILD_OPTIONS)),)
	export DEB_BUILD_OPTIONS+=parallel=$(shell getconf _NPROCESSORS_ONLN)
endif

%:
	dh $@

override_dh_auto_configure:
	# See in debian/patches/split-export.patch why we must ignore cmake return value
	-dh_auto_configure -- \
        -DFREETYPE_INCLUDE_DIR_freetype2=/usr/include/freetype2 \
        -DOCE_BUILD_SHARED_LIB:BOOL=ON \
        -DOCE_TESTING:BOOL=ON \
        -DOCE_USE_TCL_TEST_FRAMEWORK:BOOL=ON \
        -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
        -DOCE_INSTALL_PREFIX:PATH=/usr \
        -DOCE_INSTALL_LIB_DIR:PATH=lib/$(DEB_HOST_MULTIARCH) \
        -DOCE_INSTALL_CMAKE_DATA_DIR:PATH=lib/$(DEB_HOST_MULTIARCH)/oce-0.17 \
        -DOCE_DRAW:BOOL=ON \
        -DOCE_RPATH_FILTER_SYSTEM_PATHS:BOOL=ON \
        -DCMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES:STRING='/lib/$(DEB_HOST_MULTIARCH);/usr/lib/$(DEB_HOST_MULTIARCH)' \
        -DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING='$(CFLAGS)' \
        -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING='$(CXXFLAGS)' \
        -DCMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING='$(LDFLAGS)' \
        -DCMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING='$(LDFLAGS)' \
        -DOCE_MULTITHREAD_LIBRARY:STRING=NONE \
        -DOCE_WITH_FREEIMAGE:BOOL=ON \
        -DOCE_WITH_GL2PS:BOOL=ON \
        -DDART_TESTING_TIMEOUT:STRING='3600'

override_dh_auto_install:
	dh_auto_install
	-mkdir debian/tmp/usr/include/oce/$(DEB_HOST_MULTIARCH)
	mv debian/tmp/usr/include/oce/oce_build_config.defs debian/tmp/usr/include/oce/$(DEB_HOST_MULTIARCH)/oce_build_config.defs
	-mkdir debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)
	rm debian/tmp/usr/include/oce/config.h
	mv debian/tmp/usr/include/oce/oce-config.h debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/
	# Improve integration in Debian desktop
	install -d debian/tmp/usr/share/applications
	cp -p debian/oce-draw.desktop debian/tmp/usr/share/applications/
	install -d debian/tmp/usr/share/pixmaps
	cp -p debian/occ-icon-32.xpm debian/tmp/usr/share/pixmaps/
	install -d debian/tmp/usr/share/man/man1
	# Install manual page
	cp -p debian/DRAWEXE.1 debian/tmp/usr/share/man/man1/
	find debian/tmp/usr/share -name FILES -exec rm -f {} \;

override_dh_missing:
	dh_missing --fail-missing

override_dh_makeshlibs:
	dh_makeshlibs -Noce-draw

override_dh_auto_test:
	LD_LIBRARY_PATH=$(CURDIR)/$(DEBUG_PATH) dh_auto_test
